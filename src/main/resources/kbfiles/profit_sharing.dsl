PROFIT_SHARING = {
	oneOf("Überschussbeteiligung", "Überschussmitteilung zum"),
	not("eine Überschussbeteiligung erhalten),
	not("Versicherungsschein")
}